import { supabase } from '$lib/supabaseClient';
import { error } from '@sveltejs/kit';

/** @type {import('./$types').PageLoad} */
export async function load({ params }) {
	let { data, error: err } = await supabase.from('forum').select('*').eq('id', params.id).single();
	if (err) {
		throw error(500, err);
	}
	return {
		id: data.id,
		title: data.title,
		detail: data.detail,
		posted_by: data.posted_by
	};
}
